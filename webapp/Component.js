sap.ui.define([
	"sap/ui/core/UIComponent"
   ,"sap/ui/model/json/JSONModel"
   ,"sap/ui/model/resource/ResourceModel"
], function(UIComponent, JSONModel, ResourceModel){
	"use strict";
	return UIComponent.extend("sap.ui.demo.wt.Component",{
		metadata:{rootView:"sap.ui.demo.wt.view.App"}
		,init:function(){
			UIComponent.prototype.init.apply(this, arguments);
			var oData = {recipient:{
      			name:"world"
      			,buttonName:"bu_01"
  			}};
      		var oModel = new JSONModel(oData);
      		this.setModel(oModel);
		}
		});
	});